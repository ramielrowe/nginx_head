nginx_head
==========
`nginx_head` is a simple utility for generating and maintaining a nginx proxy
in front of multiple containers.

Requirements
------------
When using ICC
* Docker 1.9+
* Docker networking with an overlay network

When not using ICC
* Port needs to be exposed by the container, but you can let Docker select a random open port.

Usage
-----
Copy `deploy.sh` into your project and run it after you've updated your running
containers.

Your containers will need to have certain labels set for `deploy.sh` to pass
them to the config generator.

Exposed Labels
--------------
* com.apmelton.webhead.publish: Set to 'true' to expose.
* com.apmelton.webhead.domain: Domain and path to expose container on
  * Example: --label com.apmelton.webhead.domain=www.apmelton.com
  * Example: --label com.apmelton.webhead.domain=www.apmelton.com/docs
* com.apmelton.webhead.default: Set to 'true' to make this domain the default in nginx
* com.apmelton.webhead.icc: Set to 'true' to use inter-container communication.
* com.apmelton.webhead.icc.port: Sets an alternate port to forward traffic to when using icc.

Notes
-----
* When using `com.apmelton.webhead.icc=true` you need to set the environment
variable `OVERLAY_NETWORK` to the name of the overlay network your containers
are on.
