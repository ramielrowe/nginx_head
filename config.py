#!/usr/bin/env python

import argparse
import uuid

import jinja2

TEMPLATE="""
worker_processes {{ worker_processes }};

events {
  worker_connections 4096;
}

http {
  server_names_hash_bucket_size 64;

  {% for domain in domains.values() %}
  {% for path in domain.paths.values() %}
  upstream {{ domain.uuid }}_{{ path.uuid }} {
    zone upstream_{{ domain.uuid }}_{{ path.uuid }} 64k;
    {% for server in path.servers %}
    server {{ server.address }} weight=5;
    {% endfor %}
  }
  {% endfor %}
  server {
    listen 80 {% if domain.server_name is equalto default_server %} default_server {% endif %};
    server_name {{ domain.server_name }};
    {% for path in domain.paths.values() %}
    location {{ path.path }} {
      {% if path.path is not equalto "/" %}
      rewrite ^{{ path.path }}(.+)$ $1 break;
      rewrite ^{{ path.path }}$ / break;
      {% endif %}
      proxy_pass http://{{ domain.uuid }}_{{ path.uuid }};
      proxy_set_header Host $host;
      proxy_set_header X-Forwarded-For $remote_addr;
    }
    {% endfor %}
  }
  {% endfor %}
}
"""


class NginxObject(object):
    def __init__(self):
        self.uuid = str(uuid.uuid4()).replace('-', '')


class Domain(NginxObject):
    def __init__(self, server_name):
        super(Domain, self).__init__()
        self.server_name = server_name
        self.paths = dict()

    def get_or_create_path(self, path):
        return self.paths.setdefault(path, Path(path))


class Path(NginxObject):
    def __init__(self, path):
        super(Path, self).__init__()
        self.path = path
        self.servers = list()

    def add_server(self, server):
        self.servers.append(server)


class Server(NginxObject):
    def __init__(self, address):
        super(Server, self).__init__()
        self.address = address


def main():
    parser = argparse.ArgumentParser('nginx webhead config generator')
    parser.add_argument('-w', '--worker-processes', type=int, default=8)
    parser.add_argument('-m', '--domain-mappings', nargs='+')
    parser.add_argument('-n', '--no-rewrite', nargs='*')
    parser.add_argument('-d', '--default')
    parser.add_argument('-p', '--stdout', default=False, action='store_true')
    args = parser.parse_args()

    domains = {}

    configs = []
    for mapping in args.domain_mappings:
        (url, server) = mapping.split('=', 1)

        if '/' in url:
            (url_domain, url_path) = url.split('/', 1)
        else:
            (url_domain, url_path) = (url, '')
        url_path = '/%s' % url_path

        domain = domains.setdefault(url_domain, Domain(url_domain))
        path = domain.get_or_create_path(url_path)
        path.add_server(Server(server))

    jinja_env = jinja2.Environment()
    context = {
        'domains': domains,
        'default_server': args.default,
        'worker_processes': args.worker_processes,
    }
    config = jinja_env.from_string(TEMPLATE).render(context)

    if args.stdout:
        print config
    else:
        with open('/etc/nginx/nginx.conf', 'w') as fp:
            fp.write(config)

if __name__ == '__main__':
    main()
